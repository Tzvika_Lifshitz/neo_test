package com.Import;

import com.results.ListResult;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.procedure.Context;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Schema {

    @Context
    public GraphDatabaseService db;

    public Stream<ListResult> generateSchema() throws IOException {
        List<Object> results = new ArrayList<>();

        org.neo4j.graphdb.schema.Schema schema = db.schema();

        if (!schema.getConstraints(LabelsEnum.Door).iterator().hasNext()) {
            schema.constraintFor(LabelsEnum.Door)
                    .assertPropertyIsUnique("key")
                    .create();
            results.add("(:Door {key}) constraint created");
        }

        if (!schema.getConstraints(LabelsEnum.Port).iterator().hasNext()) {
            schema.constraintFor(LabelsEnum.Port)
                    .assertPropertyIsUnique("code")
                    .create();
            results.add("(:Port {code}) constraint created");
        }

        if (!schema.getConstraints(LabelsEnum.Carrier).iterator().hasNext()) {
            schema.constraintFor(LabelsEnum.Carrier)
                    .assertPropertyIsUnique("key")
                    .create();
            results.add("(:Carrier {key}) constraint created");
        }

        if (!schema.getConstraints(LabelsEnum.Freight).iterator().hasNext()) {
            schema.constraintFor(LabelsEnum.Freight)
                    .assertPropertyIsUnique("id")
                    .create();
            results.add("(:Freight {id}) constraint created");
        }

        if (!schema.getConstraints(LabelsEnum.LocalServiceCompany).iterator().hasNext()) {
            schema.constraintFor(LabelsEnum.LocalServiceCompany)
                    .assertPropertyIsUnique("key")
                    .create();
            results.add("(:LocalServiceCompany {key}) constraint created");
        }

        return Stream.of(new ListResult(results));
    }
}
