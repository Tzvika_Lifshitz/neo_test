package com.Import;

import org.neo4j.graphdb.RelationshipType;

public enum RelationshipTypesEnum implements RelationshipType {
    HAS_SENT,
    FREIGHT_PIECE,
    HAS_SENT_TO,
    SHIPPED_BY,
    POINTER,
    HAS_SERVICE,
    HAS_USE_SERVICE,
    SUPPLIER,
    HAS_HISTORY,
    HAS_ROAD_FREIGHTS,
    HAS_SEA_FREIGHTS,
    HAS_AIR_FREIGHTS,
    HAS_FINAL_DESTINATION,
    VIA,
    NEXT,

}
