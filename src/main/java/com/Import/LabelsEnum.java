package com.Import;


import org.neo4j.graphdb.Label;

public enum LabelsEnum implements Label {
    Leg,
    Door,
    Port,
    SeaPort,
    AirPort,

    Carrier,
    RoadCarrier,
    SeaCarrier,
    AirCarrier,

    Freight,
    RoadFreight,
    SeaFreight,
    AirFreight,

    FTL,
    LCL,
    FreightMode,
    ShipmentCategory,

    FreightPiece,

    LocalServiceCompany,
    CustomsBroker,
    Documentation,

    Destination,
    FinalDestination,

    FreightPointer
}
