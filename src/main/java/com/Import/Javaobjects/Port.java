package com.Import.Javaobjects;

import java.util.ArrayList;
import java.util.List;

public class Port {

    private String portType;
    private String code;
    private String name;
    private String countryCode;
    private String stateCode;
    private String city;
    private String departDate;
    private List<LocalService> services = new ArrayList<>();

    public Port(){}

    public String getPortType() {
        return portType;
    }

    public void setPortType(String portType) {
        this.portType = portType;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDepartDate() {
        return departDate;
    }

    public void setDepartDate(String departDate) {
        this.departDate = departDate;
    }

    public List<LocalService> getServices() {
        return services;
    }

    public void setServices(List<LocalService> services) {
        this.services = services;
    }
}
