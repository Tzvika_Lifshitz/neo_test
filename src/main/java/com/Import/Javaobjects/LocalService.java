package com.Import.Javaobjects;

public class LocalService extends ServiceProvider{
    private String serviceType;
    private String key;

    public LocalService(){

    }

    // TODO: make it smarter
    public String getServiceType() {
        return serviceType.replaceAll("\\s", "");
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void initKey(String portCode) {
        key = getName() + "_" + serviceType  + "_" + portCode;
    }
}
