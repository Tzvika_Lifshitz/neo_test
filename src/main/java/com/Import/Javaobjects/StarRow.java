package com.Import.Javaobjects;

import com.Import.JobTypeEnum;

public class StarRow {

    private JobTypeEnum jobType;
    private String mainFreightMode;
    private String hazardous;
    private String loadTerms;
    private String incoTerms;
    private Door pickupAddress;
    private Door deliveryAddress;
    private Freight freight;
    private Carrier mainFreight;
    private Carrier firstCarrier;
    private Carrier secondCarrier;
    private Port portOfLoading;
    private Port portOfDischarge;

    public StarRow() {
    }

    public JobTypeEnum getJobType() {
        return jobType;
    }

    public void setJobType(JobTypeEnum jobType) {
        this.jobType = jobType;
    }

    // TODO: write in rush, prob could b better
    public void initKeys() {
        switch (jobType) {
            case DoorToDoor:
                pickupAddress.initKey();
                deliveryAddress.initKey();
                if (mainFreightMode.equals("Road")) {
                    mainFreight.initKey(pickupAddress.getKey(), deliveryAddress.getKey(), freight.getCommodity());
                } else {
                    firstCarrier.initKey(pickupAddress.getKey(), portOfLoading.getCode(), freight.getCommodity());
                    portOfLoading.getServices().forEach(s -> s.initKey(portOfLoading.getCode()));
                    portOfDischarge.getServices().forEach(s -> s.initKey(portOfDischarge.getCode()));
                    mainFreight.initKey(portOfLoading.getCode(), portOfDischarge.getCode(), freight.getCommodity());
                    secondCarrier.initKey(portOfDischarge.getCode(), deliveryAddress.getKey(), freight.getCommodity());
                }
                break;
            case DoorToPort:
                pickupAddress.initKey();
                if (mainFreightMode.equals("Road")) {
                    portOfDischarge.getServices().forEach(s -> s.initKey(portOfDischarge.getCode()));
                    mainFreight.initKey(pickupAddress.getKey(), portOfDischarge.getCode(), freight.getCommodity());
                } else {
                    firstCarrier.initKey(pickupAddress.getKey(), portOfLoading.getCode(), freight.getCommodity());
                    portOfLoading.getServices().forEach(s -> s.initKey(portOfLoading.getCode()));
                    portOfDischarge.getServices().forEach(s -> s.initKey(portOfDischarge.getCode()));
                    mainFreight.initKey(portOfLoading.getCode(), portOfDischarge.getCode(), freight.getCommodity());
                }
                break;
            case PortToPort:
                portOfLoading.getServices().forEach(s -> s.initKey(portOfLoading.getCode()));
                portOfDischarge.getServices().forEach(s -> s.initKey(portOfDischarge.getCode()));
                mainFreight.initKey(portOfLoading.getCode(), portOfDischarge.getCode(), freight.getCommodity());
                break;
            case PortToDoor:
                portOfLoading.getServices().forEach(s -> s.initKey(portOfLoading.getCode()));
                deliveryAddress.initKey();
                if (mainFreightMode.equals("Road")) {
                    mainFreight.initKey(portOfDischarge.getCode(), pickupAddress.getKey(), freight.getCommodity());
                } else {
                    portOfDischarge.getServices().forEach(s -> s.initKey(portOfDischarge.getCode()));
                    mainFreight.initKey(portOfLoading.getCode(), portOfDischarge.getCode(), freight.getCommodity());
                    secondCarrier.initKey(portOfDischarge.getCode(), deliveryAddress.getKey(), freight.getCommodity());
                }
                break;
        }
    }

    public String getMainFreightMode() {
        return mainFreightMode;
    }

    public void setMainFreightMode(String mainFreightMode) {
        this.mainFreightMode = mainFreightMode;
    }

    public String getHazardous() {
        return hazardous;
    }

    public void setHazardous(String hazardous) {
        this.hazardous = hazardous;
    }

    public String getLoadTerms() {
        return loadTerms;
    }

    public void setLoadTerms(String loadTerms) {
        this.loadTerms = loadTerms;
    }

    public String getIncoTerms() {
        return incoTerms;
    }

    public void setIncoTerms(String incoTerms) {
        this.incoTerms = incoTerms;
    }

    public Door getPickupAddress() {
        return pickupAddress;
    }

    public void setPickupAddress(Door pickupAddress) {
        this.pickupAddress = pickupAddress;
    }

    public Door getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(Door deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public Freight getFreight() {
        return freight;
    }

    public void setFreight(Freight freight) {
        this.freight = freight;
    }

    public Carrier getMainFreight() {
        return mainFreight;
    }

    public void setMainFreight(Carrier mainFreight) {
        this.mainFreight = mainFreight;
    }

    public Carrier getFirstCarrier() {
        return firstCarrier;
    }

    public void setFirstCarrier(Carrier firstCarrier) {
        this.firstCarrier = firstCarrier;
    }

    public Carrier getSecondCarrier() {
        return secondCarrier;
    }

    public void setSecondCarrier(Carrier secondCarrier) {
        this.secondCarrier = secondCarrier;
    }

    public Port getPortOfLoading() {
        return portOfLoading;
    }

    public void setPortOfLoading(Port portOfLoading) {
        this.portOfLoading = portOfLoading;
    }

    public Port getPortOfDischarge() {
        return portOfDischarge;
    }

    public void setPortOfDischarge(Port portOfDischarge) {
        this.portOfDischarge = portOfDischarge;
    }
}
