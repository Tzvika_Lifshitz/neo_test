package com.Import.Javaobjects;

import java.util.ArrayList;
import java.util.List;

public class Freight {

    private String id;
    private String units;
    private String pickupDate;
    private String commodity;
    private String roadEta;
    private String requestedDeliveryDateFrom;
    private String requestedDeliveryDateTo;
    private String distance;
    private String status;
    private List<FreightPiece> freightPieces = new ArrayList<>();

    public Freight(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getPickupDate() {
        return pickupDate;
    }

    public void setPickupDate(String pickupDate) {
        this.pickupDate = pickupDate;
    }

    public String getCommodity() {
        return commodity;
    }

    public void setCommodity(String commodity) {
        this.commodity = commodity;
    }

    public String getRoadEta() {
        return roadEta;
    }

    public void setRoadEta(String roadEta) {
        this.roadEta = roadEta;
    }

    public String getRequestedDeliveryDateFrom() {
        return requestedDeliveryDateFrom;
    }

    public void setRequestedDeliveryDateFrom(String requestedDeliveryDateFrom) {
        this.requestedDeliveryDateFrom = requestedDeliveryDateFrom;
    }

    public String getRequestedDeliveryDateTo() {
        return requestedDeliveryDateTo;
    }

    public void setRequestedDeliveryDateTo(String requestedDeliveryDateTo) {
        this.requestedDeliveryDateTo = requestedDeliveryDateTo;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<FreightPiece> getFreightPieces() {
        return freightPieces;
    }

    public void setFreightPieces(List<FreightPiece> freightPieces) {
        this.freightPieces = freightPieces;
    }
}
