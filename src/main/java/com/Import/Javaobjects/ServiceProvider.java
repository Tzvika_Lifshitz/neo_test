package com.Import.Javaobjects;

public abstract class ServiceProvider {

    private String units;
    private String name;
    private String cost;
    private String ratePerUnit;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getRatePerUnit() {
        return ratePerUnit;
    }

    public void setRatePerUnit(String ratePerUnit) {
        this.ratePerUnit = ratePerUnit;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }
}
