package com.Import.Javaobjects;

public class Carrier extends ServiceProvider{

    private String speed;
    private String key;

    public Carrier() {
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void initKey(String originKey, String destinationKey, String commodity) {
        key = getName() + "_" + commodity + "_" + originKey + "_" + destinationKey;
    }
}
