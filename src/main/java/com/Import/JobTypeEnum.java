package com.Import;

public enum JobTypeEnum {
    DoorToDoor,
    DoorToPort,
    PortToPort,
    PortToDoor
}
