package com.Import;


import com.Import.Javaobjects.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.results.StringResult;
import org.json.simple.parser.JSONParser;
import org.neo4j.graphdb.*;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.logging.Log;
import org.neo4j.procedure.Context;

import java.io.File;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

public class Imports {

    private static final int TRANSACTION_LIMIT = 1000;
    @Context
    public Log log;
    GraphDatabaseService db =
            new GraphDatabaseFactory().newEmbeddedDatabase(new File("C:\\Users\\Tzvika\\Documents\\Neo4j\\default.graphdb"));
    JSONParser parser = new JSONParser();

    public Stream<StringResult> importRows() {
        long start = System.nanoTime();

        int count = 0;

        try (Transaction tx = db.beginTx()) {

            ObjectMapper mapper = new ObjectMapper();
            List<StarRow> starRows = mapper.readValue(new File("C:\\Users\\Tzvika\\Desktop\\stargo.json"),
                    TypeFactory.defaultInstance().constructCollectionType(List.class, StarRow.class));
            starRows.forEach(StarRow::initKeys);
            starRows.forEach(r -> {
                switch (r.getJobType()) {
                    case DoorToDoor:
                        switch (r.getMainFreightMode()) {
                            case "Road":
                                dorToDorByRoad(r);
                                break;
                            case "Sea":
                                dorToDorBySea(r);
                                break;
                            default:
                                dorToDorByAir(r);
                                break;
                        }
                        break;
                    case DoorToPort:
                        switch (r.getMainFreightMode()) {
                            case "Road":
                                dorToPortByRoad(r);
                                break;
                            case "Sea":
                                dorToPortBySea(r);
                                break;
                            default:
                                dorToPortByAir(r);
                                break;
                        }
                        break;
                    case PortToPort:
                        switch (r.getMainFreightMode()) {
                            case "Road":
                                portToPortByRoad(r);
                                break;
                            case "Sea":
                                portToPortBySea(r);
                                break;
                            default:
                                portToPortByAir(r);
                                break;
                        }
                        break;
                    case PortToDoor:
                        switch (r.getMainFreightMode()) {
                            case "Road":
                                portToDoorByRoad(r);
                                break;
                            case "Sea":
                                portToDoorBySea(r);
                                break;
                            default:
                                portToDoorByAir(r);
                                break;
                        }
                        break;
                }
            });

            tx.success();
            tx.close();

        } catch (Exception e) {
            System.out.println("Error on line: " + count);
            e.printStackTrace();
        }

        long timeTaken = TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - start);

        return Stream.of(new StringResult(count + " Rows imported in " + timeTaken + " Seconds"));
    }

    private void portToDoorByRoad(StarRow row) {

    }

    private void portToDoorBySea(StarRow row) {

    }

    private void portToDoorByAir(StarRow row) {

    }

    private void portToPortByAir(StarRow row) {

    }

    private void portToPortBySea(StarRow row) {

    }

    private void portToPortByRoad(StarRow row) {

    }

    private void dorToPortByAir(StarRow row) {

    }

    private void dorToPortBySea(StarRow row) {

    }

    private void dorToPortByRoad(StarRow row) {

    }

    private void dorToDorByAir(StarRow row) {

    }

    private void dorToDorBySea(StarRow row) {

        Node originDoor = getOrCreateDoor(row.getPickupAddress());
        Node freightNode = createFreight(row.getFreight());

        originDoor.createRelationshipTo(freightNode, RelationshipTypesEnum.HAS_SENT);

        Node portOfLoadingNode = getOrCreatePort(row.getPortOfLoading(), freightNode, false);

        freightNode.createRelationshipTo(portOfLoadingNode, RelationshipTypesEnum.HAS_SENT_TO);

        Node firstCarrierNode = getOrCreateCarrier
                (row.getFirstCarrier(), row.getFreight());

        freightNode.createRelationshipTo(firstCarrierNode, RelationshipTypesEnum.SHIPPED_BY);

        Node firstFreightPNode = findeFreightPointer(freightNode, portOfLoadingNode);

        // TODO: i like that concept
        portSendingFreight(portOfLoadingNode, firstFreightPNode);
        Node mainFreightNode = getOrCreateCarrier(row.getMainFreight(), row.getFreight());
        firstFreightPNode.createRelationshipTo(mainFreightNode, RelationshipTypesEnum.SHIPPED_BY);
        Node portOfDischargeNode = getOrCreatePort(row.getPortOfDischarge(), freightNode, false);
        firstFreightPNode.createRelationshipTo(portOfDischargeNode, RelationshipTypesEnum.HAS_SENT_TO);

        Node secondFreightPNode = findeFreightPointer(freightNode, portOfDischargeNode);

        Node destinationDoor = getOrCreateDoor(row.getDeliveryAddress());
        secondFreightPNode.createRelationshipTo(destinationDoor, RelationshipTypesEnum.HAS_SENT_TO);


    }

    private Node findeFreightPointer(Node freight, Node sendingPort) {
        Node freightPointer = null;
        for (Relationship rel:freight.getRelationships(RelationshipTypesEnum.POINTER, Direction.INCOMING)) {
            if (rel.getStartNode().getSingleRelationship(RelationshipTypesEnum.HAS_SENT, Direction.INCOMING).getStartNode().equals(sendingPort)) {
                freightPointer = rel.getStartNode();
                break;
            }
        }
        return freightPointer;
    }

    private void portSendingFreight(Node portOfLoadingNode, Node firstFreightPNode) {

    }

    private Map<Node, Boolean> getOrCreateLocalServices(Port port, Node freight) {
        Map<Node, Boolean> localServicesNodes = new HashMap<>();
        port.getServices().forEach(s -> {
            Map.Entry<Node, Boolean> localServiceEntry = getOrCreateLocalService(s, port, freight);
            localServicesNodes.put(localServiceEntry.getKey(), localServiceEntry.getValue());
        });
        return localServicesNodes;
    }

    private Map.Entry<Node, Boolean> getOrCreateLocalService(LocalService localService, Port port, Node freight) {
        Map.Entry<Node, Boolean> localServiceEntry;
        boolean isNew = false;
        Node localServiceNode = db.findNode(LabelsEnum.valueOf(localService.getServiceType()), "key", localService.getKey());
        if (localServiceNode == null) {
            isNew = true;
            localServiceNode = db.createNode(LabelsEnum.valueOf(localService.getServiceType()));
            localServiceNode.setProperty("name", localService.getName());
            localServiceNode.setProperty("key", localService.getKey());
            createHistory(localServiceNode);
        }
        Node job = createLocalServiceJob(localService);
        appendNewJob(localServiceNode, job, port.getDepartDate());
        localServiceNode.createRelationshipTo(job, RelationshipType.withName("LOG_OF_" + freight.getProperty("id")));
        localServiceEntry = new AbstractMap.SimpleEntry<>(localServiceNode, isNew);
        return localServiceEntry;
    }

    private void createHistory(Node serviceProvider) {
        Node history = db.createNode();
        history.setProperty("name", "Job_history");
        serviceProvider.createRelationshipTo(history, RelationshipType.withName("HAS_HISTORY"));
    }

    private Node getOrCreatePort(Port port, Node freight, boolean isFirstLeg) {
        Node portNode = db.findNode(LabelsEnum.Port, "code", port.getCode());
        if (portNode == null) {
            portNode = db.createNode(LabelsEnum.Port);
            portNode.setProperty("code", port.getCode());
            portNode.setProperty("portType", port.getPortType());
            portNode.setProperty("name", port.getName());
            portNode.setProperty("countryCode", port.getCountryCode());
            portNode.setProperty("city", port.getCity());
            String stateCode = port.getStateCode();
            if (!stateCode.equals("Null")) {
                portNode.setProperty("stateCode", stateCode);
            }
            addFreightModes(portNode);
        }

        Node sentFromPort;
        if (!isFirstLeg) {
            sentFromPort = db.createNode(LabelsEnum.FreightPointer);
            sentFromPort.createRelationshipTo(freight, RelationshipTypesEnum.POINTER);
        } else {
            sentFromPort = portNode;
        }

        Map<Node, Boolean> localServices = getOrCreateLocalServices(port, freight);
        for (Map.Entry<Node, Boolean> localServiceEntry : localServices.entrySet()) {

            String localServiceKey = (String) localServiceEntry.getKey().getProperty("key");
            String serviceKey = localServiceKey.substring(localServiceKey.indexOf("_") + 1);

            Node localServiceType = db.findNode(localServiceEntry.getKey().getLabels().iterator().next(), "key", serviceKey);
            // if localServiceType is null, create r
            if (localServiceType == null) {
                localServiceType = db.createNode(localServiceEntry.getKey().getLabels().iterator().next());
                localServiceType.setProperty("key", serviceKey);
                portNode.createRelationshipTo(localServiceType, RelationshipTypesEnum.HAS_SERVICE);
            }
            // if the service is new, add him
            if (localServiceEntry.getValue()) {
                localServiceType.createRelationshipTo(localServiceEntry.getKey(), RelationshipTypesEnum.SUPPLIER);
            }
            sentFromPort.createRelationshipTo(localServiceEntry.getKey(), RelationshipTypesEnum.HAS_USE_SERVICE);
        }

        return portNode;
    }

    private void addFreightModes(Node leg) {
        leg.createRelationshipTo(db.createNode(LabelsEnum.RoadFreight), RelationshipTypesEnum.HAS_ROAD_FREIGHTS);
        leg.createRelationshipTo(db.createNode(LabelsEnum.AirFreight), RelationshipTypesEnum.HAS_AIR_FREIGHTS);
        leg.createRelationshipTo(db.createNode(LabelsEnum.SeaFreight), RelationshipTypesEnum.HAS_SEA_FREIGHTS);
    }

    private void dorToDorByRoad(StarRow row) {

        Node originDoor = getOrCreateDoor(row.getPickupAddress());
        Node destinationDoor = getOrCreateDoor(row.getDeliveryAddress());

        Node freightNode = createFreight(row.getFreight());

        originDoor.createRelationshipTo(freightNode, RelationshipTypesEnum.HAS_SENT);

        Node mainFreightNode = getOrCreateCarrier
                (row.getMainFreight(), row.getFreight());

        freightNode.createRelationshipTo(mainFreightNode, RelationshipTypesEnum.SHIPPED_BY);
        freightNode.createRelationshipTo(destinationDoor, RelationshipTypesEnum.HAS_SENT_TO);

        Node roadFreights = originDoor.getSingleRelationship(RelationshipTypesEnum.HAS_ROAD_FREIGHTS, Direction.OUTGOING).getEndNode();

        // get correct load term
        Node loadType;
        String loadTypeRelName = "HAS_" + row.getLoadTerms();
        if (roadFreights.hasRelationship(RelationshipType.withName(loadTypeRelName), Direction.OUTGOING)) {
            loadType = roadFreights.getSingleRelationship(RelationshipType.withName(loadTypeRelName), Direction.OUTGOING).getEndNode();
        } else {
            loadType = db.createNode(LabelsEnum.FTL);
            roadFreights.createRelationshipTo(loadType, RelationshipType.withName(loadTypeRelName));
        }

        // add final destination
        Node finalDestinationNode = getOrCreateFinalDestination(loadType, row.getDeliveryAddress().getKey());
        finalDestinationNode.createRelationshipTo(finalDestinationNode, RelationshipTypesEnum.VIA);
        finalDestinationNode.createRelationshipTo(destinationDoor, RelationshipTypesEnum.POINTER);


    }

    private Node getOrCreateFinalDestination(Node category, String finalDestinationKey) {
        Node finalDestinationNode = null;

        for (Relationship rel : category.getRelationships(RelationshipTypesEnum.HAS_FINAL_DESTINATION)) {
            String currentKey;
            Node currentFinal = rel.getEndNode().getSingleRelationship(RelationshipTypesEnum.POINTER, Direction.OUTGOING).getEndNode();
            if (currentFinal.hasLabel(LabelsEnum.Door)) {
                currentKey = (String) currentFinal.getProperty("key");
            } else {
                currentKey = (String) currentFinal.getProperty("code");
            }

            if (currentKey.equals(finalDestinationKey)) {
                finalDestinationNode = rel.getEndNode();
                break;
            }
        }

        if (finalDestinationNode == null) {
            finalDestinationNode = db.createNode();
            category.createRelationshipTo(finalDestinationNode, RelationshipTypesEnum.HAS_FINAL_DESTINATION);
        }

        return finalDestinationNode;
    }

    private void appendNewJob(Node serviceProvider, Node job, String pickupDate) {
        Node history = serviceProvider.getSingleRelationship
                (RelationshipTypesEnum.HAS_HISTORY, Direction.OUTGOING).getEndNode();
        Relationship jobDate =
                history.getSingleRelationship(RelationshipType.withName(pickupDate), Direction.OUTGOING);
        if (jobDate == null) {
            history.createRelationshipTo(job, RelationshipType.withName(pickupDate));
        } else { // append the new job to the last job from this date
            Node lastJob = jobDate.getEndNode();
            // TODO: reduce work by insert in front
            while (lastJob.getSingleRelationship(RelationshipTypesEnum.NEXT, Direction.OUTGOING) != null) {
                lastJob = lastJob.getSingleRelationship(RelationshipTypesEnum.NEXT, Direction.OUTGOING).getEndNode();
            }
            lastJob.createRelationshipTo(job, RelationshipTypesEnum.NEXT);
        }
    }

    private Node createCarrierJob(Carrier carrier) {
        Node job = createLocalServiceJob(carrier);
        job.setProperty("speed", carrier.getSpeed());

        return job;
    }

    private Node createLocalServiceJob(ServiceProvider serviceProvider) {
        Node job = db.createNode();
        job.setProperty("cost", serviceProvider.getCost());
        job.setProperty("ratePerUnit", serviceProvider.getRatePerUnit());
        job.setProperty("units", serviceProvider.getUnits());

        return job;
    }

    private Node getOrCreateCarrier(Carrier carrier, Freight freight) {
        Node carrierNode = db.findNode(LabelsEnum.Carrier, "key", carrier.getKey());

        if (carrierNode == null) {
            carrierNode = db.createNode(LabelsEnum.Carrier);
            carrierNode.setProperty("key", carrier.getKey());
            carrierNode.setProperty("name", carrier.getName());
            createHistory(carrierNode);
        }

        Node job = createCarrierJob(carrier);
        carrierNode.createRelationshipTo(job, RelationshipType.withName("LOG_OF_" + freight.getId()));

        appendNewJob(carrierNode, job, freight.getPickupDate());
        return carrierNode;
    }

    private List<Node> createFreightPieces(Freight freight) {
        List<Node> freightPiecesNodes = new ArrayList<>();

        freight.getFreightPieces().forEach(p -> freightPiecesNodes.add(createFreightPiece(p)));
        return freightPiecesNodes;
    }

    private Node createFreightPiece(FreightPiece freightPiece) {
        Node freightPieceNode = db.createNode(LabelsEnum.FreightPiece);

        freightPieceNode.setProperty("id", freightPiece.getId());
        freightPieceNode.setProperty("type", freightPiece.getType());
        freightPieceNode.setProperty("subType", freightPiece.getSubType());
        freightPieceNode.setProperty("quantity", freightPiece.getQuantity());
        freightPieceNode.setProperty("commodity", freightPiece.getCommodity());
        freightPieceNode.setProperty("commodityDescription", (String) freightPiece.getCommodityDescription());
        freightPieceNode.setProperty("height", freightPiece.getHeight());
        freightPieceNode.setProperty("width", freightPiece.getWidth());
        freightPieceNode.setProperty("depth", freightPiece.getDepth());
        freightPieceNode.setProperty("weight", freightPiece.getWeight());
        freightPieceNode.setProperty("volume", freightPiece.getVolume());

        return freightPieceNode;
    }

    private Node getOrCreateDoor(Door door) {
        Node newDoor = db.findNode(LabelsEnum.Door, "key", door.getKey());
        if (newDoor == null) {
            newDoor = db.createNode(LabelsEnum.Door);
            newDoor.setProperty("key", door.getKey());
            newDoor.setProperty("countryName", door.getCountryName());
            newDoor.setProperty("countryCode", door.getCountryCode());
            newDoor.setProperty("stateName", door.getStateName());
            newDoor.setProperty("stateCode", door.getStateCode());
            newDoor.setProperty("city", door.getCity());
            newDoor.setProperty("address1", door.getAddress1());
            if (door.getAddress2() == null) {
                newDoor.setProperty("address2", door.getAddress2());
            }
            newDoor.setProperty("postalCode", door.getPostalCode());
            newDoor.setProperty("name", door.getName());
            addFreightModes(newDoor);
        }
        return newDoor;
    }

    private Node createFreight(Freight freight) {
        Node freightNode = db.findNode(LabelsEnum.Freight, "id", freight.getId());
        if (freightNode == null) {
            freightNode = db.createNode(LabelsEnum.Freight);

            freightNode.setProperty("id", freight.getId());
            freightNode.setProperty("units", freight.getUnits());
            freightNode.setProperty("pickupDate", freight.getPickupDate());
            freightNode.setProperty("commodity", freight.getCommodity());
            if (freight.getRoadEta() != null) {
                freightNode.setProperty("roadEta", freight.getRoadEta());
            }
            freightNode.setProperty("requestedDeliveryDateFrom", freight.getRequestedDeliveryDateFrom());
            freightNode.setProperty("requestedDeliveryDateTo", freight.getRequestedDeliveryDateTo());
            freightNode.setProperty("distance", freight.getDistance());
            freightNode.setProperty("status", freight.getStatus());
            freightNode.setProperty("requestedDeliveryDateTo", freight.getRequestedDeliveryDateTo());

            Node finalFreightNode = freightNode;
            createFreightPieces(freight).forEach(p ->
                    finalFreightNode.createRelationshipTo(p, RelationshipTypesEnum.FREIGHT_PIECE));
        }

        return freightNode;
    }
}
